'use strict';
/**
 * @ngdoc overview
 * @name bilApp
 * @description
 * # ngApp
 * 
 * v0.0.1   
 *
 * Main module of the application.
 */
angular.module('ngApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
]).config(["$routeProvider", function ($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'screens/1.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/1a', {
            templateUrl: 'screens/1a.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
        .when('/2', {
            templateUrl: 'screens/2.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/2a', {
            templateUrl: 'screens/2a.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/2b', {
            templateUrl: 'screens/2b.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
        .when('/3', {
            templateUrl: 'screens/3.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
        .when('/4', {
            templateUrl: 'screens/4.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
        .when('/5', {
            templateUrl: 'screens/5.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
        .when('/6', {
            templateUrl: 'screens/6.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/7', {
            templateUrl: 'screens/7.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/8', {
            templateUrl: 'screens/8.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/9', {
            templateUrl: 'screens/9.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/10', {
            templateUrl: 'screens/10.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
        .when('/tst', {
            templateUrl: 'views/tst.html',
            controllerAs: 'main2'
        })
        .otherwise({
            redirectTo: '/'
        });
}]);
